# Velocity distribution functions

From LAMMPS text dump files (for now)

## Setup

Need Conan, CMake and a C++17 compiler

```
cd vdf
mkdir build && cd build
conan install ..
cmake ..
make
ctest
```

Possible extra bits:
* may need to add `--build=missing` to the conan command line
* may need to convince CMake to use the right compilers - set `CMAKE_CXX_COMPILER`

