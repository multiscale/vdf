// -*- mode: c++; -*-
// Copyright (C) 2018 Rupert Nash, The University of Edinburgh

#ifndef VDF_MYGEN_H
#define VDF_MYGEN_H

#include <Eigen/Dense>

// Convience typedefs for Eigen Arrays
namespace vdf {
  using FloatArray = Eigen::Array<float, Eigen::Dynamic, 1>;
  using IdArray = Eigen::Array<int, Eigen::Dynamic, 1>;
  using Vec3Array = Eigen::Array<float, Eigen::Dynamic, 3>;
}
#endif // include guard

