// -*- mode: c++; -*-
// Copyright (C) 2018 Rupert Nash, The University of Edinburgh

#ifndef VDF_SIMPLE_READ_H
#define VDF_SIMPLE_READ_H

#include <istream>
#include <string_view>
#include "tuple_tools.h"
#include "mygen.h"

namespace vdf {
  // Simple bounding box data
  template<typename T>
  struct BBox {
    struct Range {
      T lo;
      T hi;
    };
    Range x;
    Range y;
    Range z;
  };

  struct AtomSpec {
    using LineTuple = std::tuple<int, int, float, float, float, float, float, float>;
    constexpr static const char * const names[8] = {"id", "type", "xs", "ys", "zs", "vx", "vy", "vz"};
  };

  // Header for LAMMPS dump files
  struct DumpHeader {
    int timestep;
    int nAtoms;
    BBox<float> box;
    AtomSpec spec;
  };

  // Contains the per-atom data
  struct AtomData {
    // Helper typedef for accessing an entire "row" at one
    using RowRef = tuple_add_ref<AtomSpec::LineTuple>::type;

    // Resize all arrays to the same length
    void resize(int natoms);
    // Access one "row" by reference
    RowRef operator[](unsigned i) {
      // Sanity checks
      static_assert(std::is_same_v<int&, decltype(id(i))>);
      static_assert(std::is_same_v<float&, decltype(r(i,1))>);
      return RowRef(id(i), type(i), r(i, 0), r(i, 1), r(i, 2), v(i,0), v(i,1), v(i,2));
    }
				 
    IdArray id;
    IdArray type;
    Vec3Array r;
    Vec3Array v;
  };

  // Represent a single snapshot from a dump file
  struct Dump {
    DumpHeader header;
    AtomData atoms;
  };

  // Consume some data from the stream and check it matches the string_view.
  // Return a bool indicating if it matches
  bool slurp_check(std::istream& source, const std::string_view& should_be);

  // Read a dump's header from the stream
  DumpHeader read_header(std::istream& source);

  // Read a single time's dump from the source istream
  Dump read_one(std::istream& source);

}

#endif // include guard
