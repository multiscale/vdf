// -*- mode: c++; -*-
// Copyright (C) 2018 Rupert Nash, The University of Edinburgh

#ifndef VDF_TUPLE_TOOLS_H
#define VDF_TUPLE_TOOLS_H

#include <tuple>

namespace vdf {
  // Metaprogramming helpers for tuples

  
  template <typename TupleT, typename FuncT, size_t... I>
  constexpr void tuple_for_each_impl(TupleT& t, FuncT f, std::index_sequence<I...>) {
    (f(std::get<I>(t)),...);
  }

  template <typename TupleT, typename FuncT>
  constexpr void tuple_for_each(TupleT&& t, FuncT&& f) {
    using indices = std::make_index_sequence< std::tuple_size_v<std::remove_reference_t<TupleT>>>;
    tuple_for_each_impl(std::forward<TupleT>(t), std::forward<FuncT>(f), indices{});
  }

  template <typename TupleT, template< typename > class MetaFuncT, size_t... I>
  auto tuple_meta_map_impl(std::index_sequence<I...>) -> std::tuple< typename MetaFuncT<std::tuple_element_t<I, TupleT> >::type... >;
  template <typename TupleT, template< typename > class MetaFuncT>
  struct tuple_meta_map {
    using type = decltype(tuple_meta_map_impl<TupleT, MetaFuncT>(std::make_index_sequence<std::tuple_size_v<TupleT>>{}));
  };

  // Get the index of the first element who's type is SoughtT
  template<typename SoughtT, typename TupleT>
  struct tuple_index_of;

  // Specialisation for head type matching
  template<typename SoughtT, typename... ElemTs>
  struct tuple_index_of<SoughtT, std::tuple<SoughtT, ElemTs...> > {
    static constexpr std::size_t value = 0;
  };
  // Specialisation for head type not matching (i.e. recurse into later elems)
  template<typename SoughtT, typename OtherT, typename... ElemTs>
  struct tuple_index_of<SoughtT, std::tuple<OtherT, ElemTs...> > {
    static constexpr std::size_t value = 1 +
      tuple_index_of< SoughtT, std::tuple<ElemTs...> >::value;
  };

  // Metafunction for adding a reference qualifier to every type in a
  // tuple.
  template< typename T >
  using tuple_add_ref = tuple_meta_map<T, std::add_lvalue_reference>;

  // // Main template undefined
  // template <typename TupleT>
  // struct tuple_add_ref;

  // // Specialisation for std::tuple instances
  // template <typename... T>
  // struct tuple_add_ref <std::tuple<T...>> {
  //   using type = std::tuple<std::add_lvalue_reference_t<T>...>;
  // };

}

#endif
