// -*- mode: c++; -*-
// Copyright (C) 2018 Rupert Nash, The University of Edinburgh

#ifndef VDF_VELOCITY_BINS_H
#define VDF_VELOCITY_BINS_H

#include "mygen.h"
#include <memory>

namespace vdf {
  
  enum EdgeType {
    lo,
    hi
  };

  struct BinSet {
    // number of bins
    inline int nBins() const {
      return centres.rows();
    }
    inline int nEdges() const {
      return edges.rows();
    }
    inline float Centre(int i) const {
      return centres(i);
    }
    inline float Edge(int i, EdgeType which) const {
      return edges(i + (which == lo ? 0 : 1));
    }

    FloatArray centres;
    FloatArray edges;
  };
  struct BinSet;
  
  struct BinSet3d {
    BinSet x;
    BinSet y;
    BinSet z;
  };

  struct BinSetFactory {
    static BinSet make_uniform(int nBins, float minval, float maxval);
    static BinSet3d make_uniform_3d(int nBins, float minval, float maxval);

  };

}
#endif // include guard
