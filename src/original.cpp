#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>

#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>

#include <string.h>
#include <errno.h>
#include <cmath>
using namespace std;
//-------------------------------------------------------------------
int IsFileExist(const char* path)
{
  return !access(path, F_OK);
}
//------------------------------


int IsFolderExist(const char* path)
{
  DIR *dp;
  if ((dp = opendir(path)) == NULL)
    {
      return 0;
    }

  closedir(dp);
  return -1;
}

//-----------------------------

void Display(const char *path)
{
  if (IsFolderExist(path))
    {
      printf("	Folder [%s] Exist!\n", path);
    }
  else
    {
      printf("	Folder [%s]\n", path);
      fprintf(stderr, " %s\n", strerror(errno));
    }
}
//-------------------------------------------------------------------

int main( )
{
  int m,n,k;
  const char *path;


  //----------------------------TYPES of atoms
  //----------------------------Number and ID of atoms
  const int NumAtom=20000;
  const int StartIDatomA=1;
  const int NumAtom0=20000; // from 1, 0 not inclued. Only used to define an array.

  //----------------------------
  double R=8.315;	// gas constant
  double M=39.96; // argon atom mass
  double T=298;   // temperature
  double vp; // molecular most probable velocity
  vp=sqrt(2*R*T/M*1000);
  double dpcoeff=0.2803;  // normalized pressure gradient
  //----------------------------
  const int twoD=1;
  const int threeD=1;
  double zsAmin, zsAmax;   // define the location of the slab, where the Velocity distribution analysis is done	
  zsAmin=0.0; 
  zsAmax=0.5;
  //----------------------------Dumps of atoms

  int DumpMin=0;
  int DumpMax=1000000;
  //every stepDump steps, the dumping is done.
  int stepDump=1000;
  const int NumDump=(DumpMax-DumpMin)/stepDump+1;	//Number of dumping 
  // The present value of NumDump is 1001

  // Choose the ID of the atoms to be analyzed 
  int StartAtomIDA=1;
  int EndAtomIDA=20000;
  // Choose the steps of the dumping to be analyzed (must be less than NumDump)
  int StartDumpNA=100;
  int EndDumpNA=1000;

  //---------------------------------Defination of arrays-------------------------------------------------
  const int NuSe =20; //the number of sections in each direction  // In practical use, its valuse should be about 50
  const int NuMa =21;// the number of marks in each direction     // In practical use, its valuse should be about 50
  const int NuSe_vx0 =NuSe;  
  const int NuSe_vy0 =NuSe;
  const int NuSe_vz0 =NuSe;  
  const int NuMa_vx0 =NuMa;  
  const int NuMa_vy0 =NuMa;
  const int NuMa_vz0 =NuMa;

  int Vmag=3;
  const double vx_min0  =-Vmag*vp;
  const double vy_min0  =-Vmag*vp;
  const double vz_min0  =-Vmag*vp;

  const double vx_max0  = Vmag*vp;
  const double vy_max0  = Vmag*vp;
  const double vz_max0  = Vmag*vp;
  //----------------------------

  double   **xsA;
  xsA=new double*[NumDump];
  for(m=0;m<NumDump;m++){
    xsA[m]=new double[NumAtom0];

  }
  double   **ysA;
  ysA=new double*[NumDump];
  for(m=0;m<NumDump;m++){
    ysA[m]=new double[NumAtom0];

  }
  double   **zsA;
  zsA=new double*[NumDump];
  for(m=0;m<NumDump;m++){
    zsA[m]=new double[NumAtom0];

  }
  double   **vxA;
  vxA=new double*[NumDump];
  for(m=0;m<NumDump;m++){
    vxA[m]=new double[NumAtom0];

  }
  double   **vyA;
  vyA=new double*[NumDump];
  for(m=0;m<NumDump;m++){
    vyA[m]=new double[NumAtom0];

  }
  double   **vzA;
  vzA=new double*[NumDump];
  for(m=0;m<NumDump;m++){
    vzA[m]=new double[NumAtom0];

  }
  // initialization of the arrays
  for(m=0;m<NumDump;m++) {
    for(n=1;n<NumAtom0;n++){
      xsA[m][n]=0;                
      ysA[m][n]=0;
      zsA[m][n]=0;
      vxA[m][n]=0;
      vyA[m][n]=0;
      vzA[m][n]=0;
    }
  }

  //--------------------Reading the data from file-----------------------------
  int atomID,atomID0,atomType;
  double xs,ys,zs,vx,vy,vz;

  int timestep,timestepN;
  char buffer[256];
  ifstream myfile  ("flow.lammpstrj");
  if(!myfile){
    cout << "Unable to open myfile";
    exit(1); // terminate with error
  }

  cout<<"Reading information of atoms from the file..."<<endl;
  while (!myfile.eof())
    {
      // skip the header of each section
      myfile.getline (buffer,256);  
      myfile.getline (buffer,256); 
      sscanf(buffer,"%d",&timestep);
      if (timestep%100000==0) cout<<"	Reading from the timestep:"<<timestep<<endl;  
      //Print out the timestep of the present reading
      myfile.getline (buffer,256); 
      myfile.getline (buffer,256); 
      myfile.getline (buffer,256);
      myfile.getline (buffer,256);  
      myfile.getline (buffer,256); 
      myfile.getline (buffer,256);  
      myfile.getline (buffer,256); 

      // extract data from the body of each section
      for (k=0;k<NumAtom;k++){
	myfile.getline (buffer,256); 
	sscanf(buffer,"%d %d %lf %lf %lf %lf %lf %lf",&atomID,&atomType,&xs,&ys,&zs,&vx,&vy,&vz);
	//atomID: ID of atom, from 1 to 20000
	//atomType: only argon, 1
	//xs,ys,zs: scaled coordinates in x,y,z directions
	//vx,vy,vz: velocity of atoms in x,y,z directions
		
	timestepN=timestep/stepDump; 
	//converstion from previous timpstep to the present timestepN (timestep--->timestepN)
	atomID0=atomID-(StartIDatomA-1); 
	// conversion from the previous ID to the present ID (atomID---> atomID0)
	xsA[timestepN][atomID0]=xs;
	ysA[timestepN][atomID0]=ys;
	zsA[timestepN][atomID0]=zs;
	vxA[timestepN][atomID0]=vx;
	vyA[timestepN][atomID0]=vy;
	vzA[timestepN][atomID0]=vz;
      }
    }
  cout<<"Read information completed."<<endl;
  //---------------------------------------VDF Analysis--------------------------------------------
	
  //--Create the folders and files for output of References: Marks and Centers---------------------
  cout<<"check the existence of related Folders:"<<endl;
  Display("./References");
  if(IsFileExist("./References")==0) {
    system ("mkdir ./References");
    cout<<"the file:References has been created."<<endl;         
  } 
  //-----------------------------

  path="./References/C-marks-vx.txt";
  ofstream outfile_mar_vx(path);
  path="./References/C-marks-vy.txt";
  ofstream outfile_mar_vy(path);
  path="./References/C-marks-vz.txt";
  ofstream outfile_mar_vz(path);
  //-----------------------------
  path="./References/C-cen-vx.txt";
  ofstream outfile_cen_vx(path);
  path="./References/C-cen-vy.txt";
  ofstream outfile_cen_vy(path);
  path="./References/C-cen-vz.txt";
  ofstream outfile_cen_vz(path);
  //-------------Create the folders and files for output of Normalized References-------------
  Display("./Norm_References");
  if(IsFileExist("./Norm_References")==0) {
    system ("mkdir ./Norm_References");
    cout<<"the file:Norm_References has been created."<<endl;         
  }
  path="./Norm_References/NC-marks-vx.txt";
  ofstream outfile_mar_nvx(path);
  path="./Norm_References/NC-marks-vy.txt";
  ofstream outfile_mar_nvy(path);
  path="./Norm_References/NC-marks-vz.txt";
  ofstream outfile_mar_nvz(path);
  //-----------------------------
  path="./Norm_References/NC-cen-vx.txt";
  ofstream outfile_cen_nvx(path);
  path="./Norm_References/NC-cen-vy.txt";
  ofstream outfile_cen_nvy(path);
  path="./Norm_References/NC-cen-vz.txt";
  ofstream outfile_cen_nvz(path);
  //------------Create the folders and files for the output of Results------------------------
  Display("./results");
  if(IsFileExist("./results")==0) {
    system ("mkdir ./results");
    cout<<"the file:Results has been created."<<endl;         
  }
  //---------------vx--------------
  path="./results/R-dis-vx.txt";
  ofstream outfile_dis_vx(path);
  path="./results/R-dis-vx1.txt";
  //ofstream outfile_dis_vx1(path);
  path="./results/R-dis-vx2.txt";
  //ofstream outfile_dis_vx2(path);
  //----------------vy-------------
  path="./results/R-dis-vy.txt";
  ofstream outfile_dis_vy(path);
  path="./results/R-dis-vy1.txt";
  //ofstream outfile_dis_vy1(path);
  path="./results/R-dis-vy2.txt";
  //ofstream outfile_dis_vy2(path);
  //----------------vz-------------
  path="./results/R-dis-vz.txt";
  ofstream outfile_dis_vz(path);
  path="./results/R-dis-vz1.txt";
  //ofstream outfile_dis_vz1(path);
  path="./results/R-dis-vz2.txt";
  //ofstream outfile_dis_vz2(path);
  //---------------vxvy------------	
  path="./results/R-dis-vxvy.txt";
  ofstream outfile_dis_vxvy (path);
  path="./results/R-dis-vxvy1.txt";
  //ofstream outfile_dis_vxvy1 (path);
  path="./results/R-dis-vxvy2.txt";
  //ofstream outfile_dis_vxvy2 (path);
  path="./results/R-dis-vxvy3.txt";
  //ofstream outfile_dis_vxvy3 (path);
  //---------------vxvz------------	
  path="./results/R-dis-vxvz.txt";
  ofstream outfile_dis_vxvz (path);
  path="./results/R-dis-vxvz1.txt";
  //ofstream outfile_dis_vxvz1 (path);
  path="./results/R-dis-vxvz2.txt";
  //ofstream outfile_dis_vxvz2 (path);
  path="./results/R-dis-vxvz3.txt";
  //ofstream outfile_dis_vxvz3 (path);
  //---------------vxvyvz------------	
  path="./results/R-dis-vxvyvz.txt";
  ofstream outfile_dis_vxvyvz (path);
  path="./results/R-dis-vxvyvz1.txt";
  //ofstream outfile_dis_vxvyvz1 (path);
  path="./results/R-dis-vxvyvz2.txt";
  //ofstream outfile_dis_vxvyvz2 (path);
  path="./results/R-dis-vxvyvz3.txt";
  //ofstream outfile_dis_vxvyvz3 (path);
  path="./results/R-dis-vxvyvz4.txt";
  //ofstream outfile_dis_vxvyvz4 (path);

  //--------------Create the folders and files for the output of Normalized Results-------------
  Display("./Norm_results");
  if(IsFileExist("./Norm_results")==0) {
    system ("mkdir ./Norm_results");
    cout<<"the file:Norm_Results has been created."<<endl;         
  }
  //---------------vx--------------
  path="./Norm_results/NR-dis-vx.txt";
  ofstream outfile_dis_nvx(path);
  path="./Norm_results/NR-dis-vx1.txt";
  //ofstream outfile_dis_nvx1(path);
  path="./Norm_results/NR-dis-vx2.txt";
  //ofstream outfile_dis_nvx2(path);
  //----------------vy-------------
  path="./Norm_results/NR-dis-vy.txt";
  ofstream outfile_dis_nvy(path);
  path="./Norm_results/NR-dis-vy1.txt";
  //ofstream outfile_dis_nvy1(path);
  path="./Norm_results/NR-dis-vy2.txt";
  //ofstream outfile_dis_nvy2(path);
  //----------------vz-------------
  path="./Norm_results/NR-dis-vz.txt";
  ofstream outfile_dis_nvz(path);
  path="./Norm_results/NR-dis-vz1.txt";
  //ofstream outfile_dis_nvz1(path);
  path="./Norm_results/NR-dis-vz2.txt";
  //ofstream outfile_dis_nvz2(path);
  //---------------vxvy------------	
  path="./Norm_results/NR-dis-vxvy.txt";
  ofstream outfile_dis_nvxvy (path);
  path="./Norm_results/NR-dis-vxvy1.txt";
  //ofstream outfile_dis_nvxvy1 (path);
  path="./Norm_results/NR-dis-vxvy2.txt";
  //ofstream outfile_dis_nvxvy2 (path);
  path="./Norm_results/NR-dis-vxvy3.txt";
  //ofstream outfile_dis_nvxvy3 (path);
  //---------------vxvz------------	
  path="./Norm_results/NR-dis-vxvz.txt";
  ofstream outfile_dis_nvxvz (path);
  path="./Norm_results/NR-dis-vxvz1.txt";
  //ofstream outfile_dis_nvxvz1 (path);
  path="./Norm_results/NR-dis-vxvz2.txt";
  //ofstream outfile_dis_nvxvz2 (path);
  path="./Norm_results/NR-dis-vxvz3.txt";
  //ofstream outfile_dis_nvxvz3 (path);
  //---------------vxvyvz------------	
  path="./Norm_results/NR-dis-vxvyvz.txt";
  ofstream outfile_dis_nvxvyvz (path);
  path="./Norm_results/NR-dis-vxvyvz1.txt";
  //ofstream outfile_dis_nvxvyvz1 (path);
  path="./Norm_results/NR-dis-vxvyvz2.txt";
  //ofstream outfile_dis_nvxvyvz2 (path);
  path="./Norm_results/NR-dis-vxvyvz3.txt";
  //ofstream outfile_dis_nvxvyvz3 (path);
  path="./Norm_results/NR-dis-vxvyvz4.txt";
  //ofstream outfile_dis_nvxvyvz4 (path);


  //---------------------------Marks and Centers------------------------------------
  double   vx_cen  [NuSe_vx0]={0.0};
  double   vy_cen  [NuSe_vy0]={0.0};
  double   vz_cen  [NuSe_vz0]={0.0};

  double   vx_marks[NuMa_vx0]={0.0}; // 0,1,...,NuMa_vx0-1
  double   vy_marks[NuMa_vy0]={0.0}; // 0,1,...,NuMa_vx0-1
  double   vz_marks[NuMa_vz0]={0.0}; // 0,1,...,NuMa_vx0-1
  //----------------------------Marks-----------------------------------
  for(k=0;k<NuMa_vx0;k++){
    vx_marks[k]=vx_min0+k*(vx_max0-vx_min0)/NuSe_vx0;
    outfile_mar_vx<<k<<" "<<vx_marks[k]<<endl;
    outfile_mar_nvx<<k<<" "<<vx_marks[k]/(dpcoeff*vp)<<endl;
  }

  for(k=0;k<NuMa_vy0;k++){
    vy_marks[k]=vy_min0+k*(vy_max0-vy_min0)/NuSe_vy0;
    outfile_mar_vy<<k<<" "<<vy_marks[k]<<endl;
    outfile_mar_nvy<<k<<" "<<vy_marks[k]/(dpcoeff*vp)<<endl;
  }

  for(k=0;k<NuMa_vz0;k++){
    vz_marks[k]=vz_min0+k*(vz_max0-vz_min0)/NuSe_vz0;
    outfile_mar_vz<<k<<" "<<vz_marks[k]<<endl;
    outfile_mar_nvz<<k<<" "<<vz_marks[k]/(dpcoeff*vp)<<endl;
  }
	
  //--------------------------- Centers-----------------------
  for(k=0;k<NuSe_vx0;k++){
    vx_cen[k]   =vx_min0+(0.5+k)*(vx_max0-vx_min0)/NuSe_vx0;
    outfile_cen_vx<<k<<" "<<vx_cen[k]<<endl;
    outfile_cen_nvx<<k<<" "<<vx_cen[k]/(dpcoeff*vp)<<endl;
  }

  for(k=0;k<NuSe_vy0;k++){
    vy_cen[k]   =vy_min0+(0.5+k)*(vy_max0-vy_min0)/NuSe_vy0;
    outfile_cen_vy<<k<<" "<<vy_cen[k]<<endl;
    outfile_cen_nvy<<k<<" "<<vy_cen[k]/(dpcoeff*vp)<<endl;
  }

  for(k=0;k<NuSe_vz0;k++){
    vz_cen[k]   =vz_min0+(0.5+k)*(vz_max0-vz_min0)/NuSe_vz0;
    outfile_cen_vz<<k<<" "<<vz_cen[k]<<endl;
    outfile_cen_nvz<<k<<" "<<vz_cen[k]/(dpcoeff*vp)<<endl;
  }

  //---------------------------Dis and Pros------------------------------------
  double   vx_dis    [NuSe_vx0]={0.0};
  double   vy_dis    [NuSe_vx0]={0.0};
  double   vz_dis    [NuSe_vz0]={0.0};
  double   provxdis[NuSe_vx0]={0.0};
  double   provydis[NuSe_vy0]={0.0};
  double   provzdis[NuSe_vz0]={0.0};
  /////////////////////
  double   vxvy_dis  [NuSe_vx0][NuSe_vy0]={0.0};
  double   vxvz_dis  [NuSe_vx0][NuSe_vz0]={0.0};
  double   provxvydis [NuSe_vx0][NuSe_vy0] = {0.0};
  double   provxvzdis [NuSe_vx0][NuSe_vz0] = {0.0};
  /////////////////////
  //double   vxvyvz_dis  [NuSe_vx0][NuSe_vy0][NuSe_vz0]={0.0};
  //double   provxvyvzdis [NuSe_vx0][NuSe_vy0][NuSe_vz0] = {0.0};
	
  int i,j;
  double *** vxvyvz_dis = new double **[NuSe_vx0]();
  for (i = 0; i <= NuSe_vx0; i++)
    {
      vxvyvz_dis[i] = new double *[NuSe_vy0]();
      for (j = 0; j <= NuSe_vy0; j++)
	vxvyvz_dis[i][j] = new double [NuSe_vz0]();
    }

  double *** provxvyvzdis= new double **[NuSe_vx0]();
  for (i = 0; i <= NuSe_vx0; i++)
    {
      provxvyvzdis[i] = new double *[NuSe_vy0]();
      for (j = 0; j <= NuSe_vy0; j++)
	provxvyvzdis[i][j] = new double [NuSe_vz0]();
    }

  for (m=0;m<=NuSe_vx0;m++){
    for (n=0;n<=NuSe_vx0;n++){
      for (i=0;i<=NuSe_vx0;i++){
	vxvyvz_dis[m][n][i]=0;
	provxvyvzdis[m][n][i]=0;
      }
    }
  }
	
  //**********************************************************************************************
  //*************In practical use,the 2D and 3D analysis in the below part cause a high cost of compuation time.
  //*************Two reasons:
  //*************1)when NuSe nad NuMa are chosen as a large value, e.g.40.
  //*************which could lead to large values of Nuse_vx0, NuS_vy0, NuSe_Vz0.
  //*************2)Practical simulation will use a large value for DumpMax, which leads to a large value of EndDumpNA,
  //*************corresponding to an large size of datafile, e.g. about 50GB.
  //**********************************************************************************************

  cout<<"start the analysis:"<<endl;
  int px=0,py=0,pz=0;
  int q1=0;	

  for(m=StartDumpNA;m<=EndDumpNA;m++){  // start from StartDumpNA, and end at EndDumpNA
    if(m%10==0) cout<<"	Analysing the timestep:"<<m<<endl;
    //  identify the type of atoms to analyze
    for(n=StartAtomIDA;n<=EndAtomIDA;n++){		
      if (zsA[m][n]>zsAmin && zsA[m][n]<zsAmax){   	   
	//	zsAmin=0.0; 
	//	zsAmax=0.013888;	     
	vxA[m][n]=vxA[m][n]*100;
	vyA[m][n]=vyA[m][n]*100;
	vzA[m][n]=vzA[m][n]*100;       	    
	q1++;	
	//////////////////////////////////////////////   1-D analysis
	//x direction
	for(px=0;px<NuSe_vx0;px++){
	  if((vx_marks[px]  <=vxA[m][n])&&
	     vx_marks[px+1]> vxA[m][n]){
	    vx_dis[px]    = vx_dis[px]+1;
	  }
	}
	                    	
	//y direction
	for(py=0;py<NuSe_vy0;py++){
	  if((vy_marks[py]  <=vyA[m][n])&&
	     vy_marks[py+1]> vyA[m][n]){
	    vy_dis[py]    = vy_dis[py]+1;
	  }
	}
        	           
	// only z direction
	for(pz=0;pz<NuSe_vz0;pz++){
	  if((vz_marks[pz]  <=vzA[m][n])&&
	     vz_marks[pz+1]> vzA[m][n]){
	    vz_dis[pz]    = vz_dis[pz]+1;
	  }
	}
	//////////////////////////////////////////////   1-D analysis end

	//////////////////////////////////////////////   2-D analysis
	if(twoD==1){  // if twoD==0, no 2D analysis
	  // x and y direction
	  for(px=0;px<NuSe_vx0;px++){
	    for(py=0;py<NuSe_vy0;py++){
	      if(vx_marks[px]  <=vxA[m][n] &&
		 vx_marks[px+1]> vxA[m][n] &&
		 vy_marks[py]  <=vyA[m][n] &&
		 vy_marks[py+1]> vyA[m][n]){
		vxvy_dis[px][py]=vxvy_dis[px][py]+1;
	      } // end if
	    } //end for py
	  } // end for px

	  // x and z direction
	  for(px=0;px<NuSe_vx0;px++){
	    for(pz=0;pz<NuSe_vz0;pz++){
	      if(vx_marks[px]  <=vxA[m][n] &&
		 vx_marks[px+1]> vxA[m][n] &&
		 vz_marks[pz]  <=vzA[m][n] &&
		 vz_marks[pz+1]> vzA[m][n]){
		vxvz_dis[px][pz]=vxvz_dis[px][pz]+1;
	      } // end if
	    } //end for pz
	  } // end for px
	}// end for 2D
	//////////////////////////////////////////////   2-D analysis end
		
	//////////////////////////////////////////////   3-D analysis
	if(threeD==1){ // if threeD==0, no 3D analysis
	  for(px=0;px<NuSe_vx0;px++){
	    for(py=0;py<NuSe_vy0;py++){
	      for(pz=0;pz<NuSe_vz0;pz++){
		if(vx_marks[px]  <=vxA[m][n] &&
		   vx_marks[px+1]> vxA[m][n] &&
		   vy_marks[py]  <=vyA[m][n] &&
		   vy_marks[py+1]> vyA[m][n] &&
		   vz_marks[pz]  <=vzA[m][n] &&
		   vz_marks[pz+1]> vzA[m][n]){
		  vxvyvz_dis[px][py][pz]=vxvyvz_dis[px][py][pz]+1;
		} // end if
	      } //end for pz
	    } // end for py
	  } // end for px
	}	// end for 3D 
	//////////////////////////////////////////////   3-D analysis end
      } // if (zsA[m][n]>zsAmin && zsA[m][n]<zsAmax){ 
    }  // for(n=StartAtomID;n<=EndAtomID;n++){	
  }  // end from StartDumpNA, and end at EndDumpNA

  //**********************************************************************************************
  //**********************************************************************************************
  //**********************************************************************************************
  //**********************************************************************************************
  //**********************************************************************************************

  // calculate the probility according to the above statistics
  //-------------  1-D analysis   	
  for(px=0;px<NuSe_vx0;px++){
    provxdis[px]=vx_dis[px]/q1/((vx_max0-vx_min0)/NuSe_vx0);
  }

  for(py=0;py<NuSe_vy0;py++){
    provydis[py]=vy_dis[py]/q1/((vy_max0-vy_min0)/NuSe_vy0);
  }

  for(pz=0;pz<NuSe_vz0;pz++){
    provzdis[pz]=vz_dis[pz]/q1/((vz_max0-vz_min0)/NuSe_vz0);
  }
	
  //-------------  2-D analysis
  if(twoD==1){
    for(px=0;px<NuSe_vx0;px++){
      for(py=0;py<NuSe_vy0;py++){
	provxvydis[px][py]=vxvy_dis[px][py]/q1/((vx_max0-vx_min0)/NuSe_vx0)/((vy_max0-vy_min0)/NuSe_vy0);
      }
    }

    for(px=0;px<NuSe_vx0;px++){
      for(pz=0;pz<NuSe_vz0;pz++){
	provxvzdis[px][pz]=vxvz_dis[px][pz]/q1/((vx_max0-vx_min0)/NuSe_vx0)/((vz_max0-vz_min0)/NuSe_vz0);
      }
    }
  }  //	if(twoD==1){

  //-------------  3-D analysis
  if(threeD==1){
    for(px=0;px<NuSe_vx0;px++){
      for(py=0;py<NuSe_vy0;py++){
	for(pz=0;pz<NuSe_vz0;pz++){
	  provxvyvzdis[px][py][pz]=vxvyvz_dis[px][py][pz]/q1/((vx_max0-vx_min0)/NuSe_vx0)/((vy_max0-vy_min0)/NuSe_vy0)/((vz_max0-vz_min0)/NuSe_vz0);
	}
      }
    }
  }	
  cout<<"The analysis completed."<<endl;	

  //***************************************out put********************************************
  //-------------------------1D
  cout<<"The output begins:"<<endl;
  for(k=0;k<NuSe_vx0;k++){
    outfile_dis_vx<<fixed <<vx_cen[k]<<"    "<<provxdis[k]<<endl;
    //outfile_dis_vx1<<fixed << vx_cen[k]<<endl;
    //outfile_dis_vx2<<fixed <<provxdis[k]<<endl;
	
    outfile_dis_nvx<<fixed <<vx_cen[k]/(dpcoeff*vp)<<"    "<<provxdis[k]*(dpcoeff*vp)<<endl;
    //outfile_dis_nvx1<<fixed << vx_cen[k]/(dpcoeff*vp)<<endl;
    //outfile_dis_nvx2<<fixed <<provxdis[k]*(dpcoeff*vp)<<endl;	   
  }

  for(k=0;k<NuSe_vy0;k++){
    outfile_dis_vy<<fixed <<vy_cen[k]<<"    "<<provydis[k]<<endl;
    //outfile_dis_vy1<<fixed << vy_cen[k]<<endl;
    //outfile_dis_vy2<<fixed <<provydis[k]<<endl;

    outfile_dis_nvy<<fixed <<vy_cen[k]/(dpcoeff*vp)<<"    "<<provydis[k]*(dpcoeff*vp)<<endl;
    //outfile_dis_nvy1<<fixed << vy_cen[k]/(dpcoeff*vp)<<endl;
    //outfile_dis_nvy2<<fixed <<provydis[k]*(dpcoeff*vp)<<endl;
  }

  for(k=0;k<NuSe_vz0;k++){
    outfile_dis_vz <<fixed  <<vz_cen[k]<<"    "<<provzdis[k]<<endl;
    //outfile_dis_vz1<<fixed << vz_cen[k]<<endl;
    //outfile_dis_vz2<<fixed <<provzdis[k]<<endl;

    outfile_dis_nvz <<fixed  <<vz_cen[k]/(dpcoeff*vp)<<"    "<<provzdis[k]*(dpcoeff*vp)<<endl;
    //outfile_dis_nvz1<<fixed << vz_cen[k]/(dpcoeff*vp)<<endl;
    //outfile_dis_nvz2<<fixed <<provzdis[k]*(dpcoeff*vp)<<endl;
  }
  //-------------------------2D
  if(twoD==1){
    for(px=0;px<NuSe_vx0;px++){
      for(py=0;py<NuSe_vy0;py++){
	outfile_dis_vxvy<<vx_cen[px]<<"    "<<vy_cen[py]<<"    "<<provxvydis[px][py]<<endl;
	//outfile_dis_vxvy1<<vx_cen[px]<<endl;
	//outfile_dis_vxvy2<<vy_cen[py]<<endl;
	//outfile_dis_vxvy3<<provxvydis[px][py]<<endl;
	//---------------------------------
	outfile_dis_nvxvy<<vx_cen[px]/(dpcoeff*vp)<<"    "<<vy_cen[py]/(dpcoeff*vp)<<"    "<<provxvydis[px][py]*(dpcoeff*vp)*(dpcoeff*vp)<<endl;
	//outfile_dis_nvxvy1<<vx_cen[px]/(dpcoeff*vp)<<endl;
	//outfile_dis_nvxvy2<<vy_cen[py]/(dpcoeff*vp)<<endl;
	//outfile_dis_nvxvy3<<provxvydis[px][py]*(dpcoeff*vp)*(dpcoeff*vp)<<endl;
      }
    }

    for(px=0;px<NuSe_vx0;px++){
      for(pz=0;pz<NuSe_vz0;pz++){
	outfile_dis_vxvz<<vx_cen[px]<<"    "<<vz_cen[pz]<<"    "<<provxvzdis[px][pz]<<endl;
	//outfile_dis_vxvz1<<vx_cen[px]<<endl;
	//outfile_dis_vxvz2<<vz_cen[pz]<<endl;
	//outfile_dis_vxvz3<<provxvzdis[px][pz]<<endl;
	//---------------------------------
	outfile_dis_nvxvz<<vx_cen[px]/(dpcoeff*vp)<<"    "<<vz_cen[pz]/(dpcoeff*vp)<<"    "<<provxvzdis[px][pz]*(dpcoeff*vp)*(dpcoeff*vp)<<endl;
	//outfile_dis_nvxvz1<<vx_cen[px]/(dpcoeff*vp)<<endl;
	//outfile_dis_nvxvz2<<vz_cen[pz]/(dpcoeff*vp)<<endl;
	//outfile_dis_nvxvz3<<provxvzdis[px][pz]*(dpcoeff*vp)*(dpcoeff*vp)<<endl;
      }
    }
  }
  //------------------------3D
  if(threeD==1){
    for(px=0;px<NuSe_vx0;px++){
      for(py=0;py<NuSe_vy0;py++){
	for(pz=0;pz<NuSe_vz0;pz++){
	  outfile_dis_vxvyvz<<vx_cen[px]<<"    "<<vy_cen[py]<<"    "<<vz_cen[pz]<<"    "<<provxvyvzdis[px][py][pz]<<endl;
	  //outfile_dis_vxvyvz1<<vx_cen[px]<<endl;
	  //outfile_dis_vxvyvz2<<vy_cen[py]<<endl;
	  //outfile_dis_vxvyvz3<<vz_cen[pz]<<endl;
	  //outfile_dis_vxvyvz4<<provxvyvzdis[px][py][pz]<<endl;
	  //---------------------------------
	  outfile_dis_nvxvyvz <<vx_cen[px]/(dpcoeff*vp)<<"    "<<vy_cen[py]/(dpcoeff*vp)<<"    "<<vz_cen[pz]/(dpcoeff*vp)
			      <<"    "<<provxvyvzdis[px][py][pz]*(dpcoeff*vp)*(dpcoeff*vp)*(dpcoeff*vp)<<endl;
	  //outfile_dis_nvxvyvz1<<vx_cen[px]/(dpcoeff*vp)<<endl;
	  //outfile_dis_nvxvyvz2<<vy_cen[py]/(dpcoeff*vp)<<endl;
	  //outfile_dis_nvxvyvz3<<vz_cen[pz]/(dpcoeff*vp)<<endl;
	  //outfile_dis_nvxvyvz4<<provxvyvzdis[px][py][pz]*(dpcoeff*vp)*(dpcoeff*vp)*(dpcoeff*vp)<<endl;
	}
      }
    }
  }
  cout<<"The output completed."<<endl;
  //*********************************************************************************************


  //----------------------------xsA---------
  for (m=0;m<NumDump;m++)
    {
      delete[] xsA[m];
    }
  delete[] xsA;
  xsA=NULL;
  //----------------------------ysA---------
  for (m=0;m<NumDump;m++)
    {
      delete[] ysA[m];
    }
  delete[] ysA;
  ysA=NULL;
  //----------------------------zsA---------
  for (m=0;m<NumDump;m++)
    {
      delete[] zsA[m];
    }
  delete[] zsA;
  zsA=NULL;
  //----------------------------vxA---------
  for (m=0;m<NumDump;m++)
    {
      delete[] vxA[m];
    }
  delete[] vxA;
  vxA=NULL;
  //----------------------------vyA---------
  for (m=0;m<NumDump;m++)
    {
      delete[] vyA[m];
    }
  delete[] vyA;
  vyA=NULL;
  //----------------------------vzA---------
  for (m=0;m<NumDump;m++)
    {
      delete[] vzA[m];
    }
  delete[] vzA;
  vzA=NULL;
  //----------------------------provxvyvzdis---------
  for (i = 0; i < NuSe_vx0; i++)
    {
      for (j = 0; j < NuSe_vy0; j++)
	delete[] provxvyvzdis[i][j];
      delete[] provxvyvzdis[i];
    }
  delete[] provxvyvzdis;
  //----------------------------vxvyvz_dis---------
  for (i = 0; i < NuSe_vx0; i++)
    {
      for (j = 0; j < NuSe_vy0; j++)
	delete[]vxvyvz_dis[i][j];
      delete[] vxvyvz_dis[i];
    }
  delete[] vxvyvz_dis;
  return 0;
}






