// Copyright (C) 2018 Qiang Sheng & Rupert Nash, The University of Edinburgh

#include <fmt/format.h>
#include <charconv>
#include "simple_read.h"
#include "tuple_tools.h"

namespace vdf {

  bool slurp_check(std::istream& source, const std::string_view& expected) {
    const auto n = expected.size();
    auto buf = std::make_unique<char[]>(n);
    // istream::get reads until you read N-1 chars or hit the sentinal
    source.get(buf.get(), n+1, '\0');

    auto first_diff = std::mismatch(expected.begin(), expected.end(),
				    buf.get());
    return (first_diff.first == expected.end());
  }
  void slurp_throw(std::istream& source, const std::string_view& expected) {
    if (!slurp_check(source, expected))
      throw std::runtime_error(fmt::format("Expected '{}'", expected));
  }

  void assert_next(std::istream& source, char c) {
    char read;
    source.get(read);
    if (read != c)
      throw std::runtime_error(fmt::format("Wanted a '{}' got a '{}'", c, read));
  }

  DumpHeader read_header(std::istream& source) {
    DumpHeader ans;

    slurp_throw(source,  "ITEM: TIMESTEP\n");
    source >> ans.timestep;
  
    slurp_throw(source, "\nITEM: NUMBER OF ATOMS\n");
    source >> ans.nAtoms;

    slurp_throw(source, "\nITEM: BOX BOUNDS ");
    // Now expect typecodes for boundaries "xx yy zz"
    // periodic - p
    // fixed - f
    // shrink wrap - s
    // shrink wrap with min val - m 
    {
      auto check = [&]() {
	char read;
	source >> read;
	if (read != 'p' && read != 'f' && read != 's' && read && 'm')
	  throw std::runtime_error(fmt::format("Reading box bound type code expected one of 'pfsm' but got '{}'", read));
      };
      check(); check();
      assert_next(source, ' ');
      check(); check();
      assert_next(source, ' ');
      check(); check();
      assert_next(source, '\n');
    }
    // Now read box size
    source >> ans.box.x.lo >> ans.box.x.hi
	   >> ans.box.y.lo >> ans.box.y.hi
	   >> ans.box.z.lo >> ans.box.z.hi;

    // Make the expected atom specifier text
    slurp_throw(source,  "\nITEM: ATOMS ");
    for (auto name: AtomSpec::names) {
      slurp_throw(source, name);
      assert_next(source, ' ');
    }
    assert_next(source, '\n');
    return ans;
  }

  void AtomData::resize(int natoms) {
    id.resize(natoms, 1);
    type.resize(natoms, 1);
    r.resize(natoms, 3);
    v.resize(natoms, 3);
  }

  Dump read_one(std::istream& source) {
    Dump ans;
    ans.header = read_header(source);
    const auto& N = ans.header.nAtoms;
    ans.atoms.resize(N);
  
    for (auto i = 0; i < N; ++i) {
      AtomSpec::LineTuple extracted_line;
      tuple_for_each(extracted_line, [&](auto& dest) {
	  source >> dest;
	  assert_next(source, ' ');	  
	});
    
      assert_next(source, '\n');
      auto atomIndex = std::get<0>(extracted_line) - 1;
      ans.atoms[atomIndex] = extracted_line;
    }
    return ans;
  }

}
