#include "velocity_bins.h"

namespace vdf {
  
  BinSet BinSetFactory::make_uniform(int nBins, float minval, float maxval) {
    BinSet ans;
    ans.centres.resize(nBins);
    ans.edges.resize(nBins+1);
    auto range = maxval - minval;
    auto delta = range / nBins;

    ans.edges(0) = minval;
    for (auto k = 0; k < nBins; ++k) {
      ans.edges(k+1) = minval + (k + 1) * delta;
      ans.centres(k) = 0.5 * (ans.edges(k) + ans.edges(k+1));
    }
    return ans;
  }

  BinSet3d BinSetFactory::make_uniform_3d(int nBins, float minval, float maxval) {
    return BinSet3d{
      make_uniform(nBins, minval, maxval),
	make_uniform(nBins, minval, maxval),
	make_uniform(nBins, minval, maxval)
	};
  }

}
