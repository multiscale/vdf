// Copyright (C) 2018 Rupert Nash, Qiang Sheng, The University of Edinburgh

#include <catch2/catch.hpp>
#include "velocity_bins.h"
#include <cmath>

using namespace vdf;

SCENARIO("New style bin data matches original", "[bins]") {
  GIVEN("An extract of the original code") {
    double R=8.315;	// gas constant
    double M=39.96; // argon atom mass
    double T=298;   // temperature
    double vp=std::sqrt(2*R*T/M*1000); // molecular most probable velocity
    const int NuSe =20; //the number of sections in each direction  // In practical use, its valuse should be about 50
    const int NuMa =21;// the number of marks in each direction     // In practical use, its valuse should be about 50
    const int NuSe_vx0 =NuSe;  
    const int NuSe_vy0 =NuSe;
    const int NuSe_vz0 =NuSe;  
    const int NuMa_vx0 =NuMa;  
    const int NuMa_vy0 =NuMa;
    const int NuMa_vz0 =NuMa;

    int Vmag=3;
    const double vx_min0  =-Vmag*vp;
    const double vy_min0  =-Vmag*vp;
    const double vz_min0  =-Vmag*vp;

    const double vx_max0  = Vmag*vp;
    const double vy_max0  = Vmag*vp;
    const double vz_max0  = Vmag*vp;
    //----------------------------
    //---------------------------Marks and Centers------------------------------------
    double   vx_cen  [NuSe_vx0]={0.0};
    double   vy_cen  [NuSe_vy0]={0.0};
    double   vz_cen  [NuSe_vz0]={0.0};

    double   vx_marks[NuMa_vx0]={0.0}; // 0,1,...,NuMa_vx0-1
    double   vy_marks[NuMa_vy0]={0.0}; // 0,1,...,NuMa_vx0-1
    double   vz_marks[NuMa_vz0]={0.0}; // 0,1,...,NuMa_vx0-1
    //----------------------------Marks-----------------------------------
    for(auto k=0;k<NuMa_vx0;k++){
      vx_marks[k]=vx_min0+k*(vx_max0-vx_min0)/NuSe_vx0;
    }

    for(auto k=0;k<NuMa_vy0;k++){
      vy_marks[k]=vy_min0+k*(vy_max0-vy_min0)/NuSe_vy0;
    }

    for(auto k=0;k<NuMa_vz0;k++){
      vz_marks[k]=vz_min0+k*(vz_max0-vz_min0)/NuSe_vz0;
    }  
    //--------------------------- Centers-----------------------
    for(auto k=0;k<NuSe_vx0;k++){
      vx_cen[k]   =vx_min0+(0.5+k)*(vx_max0-vx_min0)/NuSe_vx0;
    }

    for(auto k=0;k<NuSe_vy0;k++){
      vy_cen[k]   =vy_min0+(0.5+k)*(vy_max0-vy_min0)/NuSe_vy0;
    }

    for(auto k=0;k<NuSe_vz0;k++){
      vz_cen[k]   =vz_min0+(0.5+k)*(vz_max0-vz_min0)/NuSe_vz0;
    }

    WHEN("New code creates the same bins") {
      auto bins = BinSetFactory::make_uniform_3d(NuSe, -Vmag*vp, +Vmag*vp);

      THEN("They should be the same") {
	for (auto i = 0; i < NuSe; ++i) {
	  CHECK(bins.x.Edge(i, lo) == Approx(vx_marks[i]));
	  CHECK(bins.y.Edge(i, lo) == Approx(vy_marks[i]));
	  CHECK(bins.z.Edge(i, lo) == Approx(vz_marks[i]));

	  CHECK(bins.x.Centre(i) == Approx(vx_cen[i]));
	  CHECK(bins.y.Centre(i) == Approx(vy_cen[i]));
	  CHECK(bins.z.Centre(i) == Approx(vz_cen[i]));

	  CHECK(bins.x.Edge(i, hi) == Approx(vx_marks[i+1]));
	  CHECK(bins.y.Edge(i, hi) == Approx(vy_marks[i+1]));
	  CHECK(bins.z.Edge(i, hi) == Approx(vz_marks[i+1]));
	}
      }
    }
  }
}
