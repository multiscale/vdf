// Copyright (C) 2018 Rupert Nash, The University of Edinburgh

#include <sstream>
#include <catch2/catch.hpp>
#include "simple_read.h"

using namespace vdf;

SCENARIO("Consumption of unused strings workds", "[parse]") {
  GIVEN("An input stream containing some text") {
    std::istringstream stream("Some text");
    WHEN("checked against a char*") {
      auto ipos = stream.tellg();
      auto result = slurp_check(stream, "Some");
      THEN("it should return true"){
	REQUIRE(result);
      }
      AND_THEN("the stream position is advanced") {
	REQUIRE(stream.tellg() == (ipos + 4LL));
      }
    }
  }
}
      
namespace {
  const std::string tiny_dump(R"raw(ITEM: TIMESTEP
0
ITEM: NUMBER OF ATOMS
5
ITEM: BOX BOUNDS pp pp ff
0.0000000000000000e+00 5.0940000000000003e+02
0.0000000000000000e+00 5.0940000000000003e+02
0.0000000000000000e+00 3.6000000000000000e+02
ITEM: ATOMS id type xs ys zs vx vy vz 
1 1 0.141346 0.141346 0.0833375 0 0 0 
2 1 0.153121 0.141346 0.0833377 0 0 0 
3 1 0.1649 0.141346 0.0833377 0 0 0 
4 1 0.176678 0.141346 0.0833377 0 0 0 
5 1 0.188457 0.141346 0.0833377 0 0 0 
)raw");

  const std::string ooo_dump(R"raw(ITEM: TIMESTEP
1000
ITEM: NUMBER OF ATOMS
5
ITEM: BOX BOUNDS pp pp ff
0.0000000000000000e+00 5.0940000000000003e+02
0.0000000000000000e+00 5.0940000000000003e+02
0.0000000000000000e+00 3.6000000000000000e+02
ITEM: ATOMS id type xs ys zs vx vy vz 
2 1 0.158093 0.145886 0.0896292 -0.137263 0.843997 0.817599 
5 1 0.188516 0.145016 0.0883952 0.0584924 0.431682 0.404443 
3 1 0.166638 0.145103 0.0885175 2.19447 0.414986 0.387021 
1 1 0.144357 0.144357 0.0874612 0.207277 0.207277 0.181168 
4 1 0.176194 0.145051 0.0884446 -0.370537 0.453782 0.426465 
)raw");
}

SCENARIO("Headers parse", "[parse]") {
  GIVEN("A small correct dump") {
    
    std::istringstream stream(tiny_dump);
    WHEN("The headers are read") {
      auto headers = read_header(stream);
      THEN("Timestep is correct") {
	CHECK(headers.timestep == 0);
      }
      AND_THEN("Number of atoms is correct") {
	CHECK(headers.nAtoms == 5);
      }
      AND_THEN("Box bounds are about right") {
	CHECK(headers.box.x.lo == Approx(0.0));
	CHECK(headers.box.y.lo == Approx(0.0));
	CHECK(headers.box.z.lo == Approx(0.0));
	
	CHECK(headers.box.x.hi == Approx(509.4));
	CHECK(headers.box.y.hi == Approx(509.4));
	CHECK(headers.box.z.hi == Approx(360.0));

      }
    }
  }
}

SCENARIO("Atoms parse", "[parse]") {
  GIVEN("A small correct dump") {
    std::istringstream stream(tiny_dump);

    WHEN("it is read") {
      auto dump = read_one(stream);

      THEN("There's nothing left") {
	CHECK(stream.tellg() == tiny_dump.size());
      }

      AND_THEN("ID[i] == i + 1 for all i") {
	for(auto i=0; i < dump.header.nAtoms; ++i) {
	  CHECK(dump.atoms.id(i) == i+1);
	}
      }

      AND_THEN("a few positions are correct") {
	CHECK(dump.atoms.r(0, 0) == Approx(0.141346));
	CHECK(dump.atoms.r(1, 2) == Approx(0.0833377));
	CHECK(dump.atoms.r(3, 1) == Approx(0.141346));
      }
      
      AND_THEN("all velocities are zero") {
	CHECK((dump.atoms.v == 0.0).all());
      }
    }
  }

  GIVEN("A small out of order dump") {
    std::istringstream stream(ooo_dump);

    WHEN("it is read") {
      auto dump = read_one(stream);
      THEN("There's nothing left") {
	CHECK(stream.tellg() == ooo_dump.size());
      }

      AND_THEN("ID[i] == i + 1 for all i") {
	for(auto i=0; i < dump.header.nAtoms; ++i) {
	  CHECK(dump.atoms.id(i) == i+1);
	}
      }
    }
  }

  GIVEN("A two-timestep dump") {
    std::istringstream stream(tiny_dump + ooo_dump);
    WHEN("it is read") {
      auto dump0 = read_one(stream);

      THEN("it can be read again") {
	auto dump1 = read_one(stream);
	REQUIRE(dump1.header.timestep == 1000);
      }
    }
  }
}
